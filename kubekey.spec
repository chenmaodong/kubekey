%define debug_package %{nil}

Name:        kubekey
Summary:     Install Kubernetes/K3s only, both Kubernetes/K3s and KubeSphere, and related cloud-native add-ons, it supports all-in-one, multi-node, and HA.
Version:     v2.2.2
Release:     3
License:     Apache-2.0
URL:         https://github.com/kubesphere/kubekey

Source0: https://github.com/kubesphere/kubekey/releases/download/%{version}-openEuler/kubekey-%{version}.tar.gz
Patch0001:   backport-0001-fix-invalid-syntax-of-etc_sysctl_conf.patch
Patch0002:   backport-0001-fix-delete-k3s-cluster-with-kubenetes-version.patch

BuildRequires:  git go
Requires:       socat
Requires:       conntrack


%description
Install Kubernetes/K3s only, both Kubernetes/K3s and KubeSphere, and related cloud-native add-ons, it supports all-in-one, multi-node, and HA.

%prep
%autosetup -n %{name}-%{version} -p1

%build
bash -x build.sh -p

%install
mkdir -p $RPM_BUILD_ROOT/usr/local/bin
cp output/kk $RPM_BUILD_ROOT/usr/local/bin

%files
/usr/local/bin/kk


%changelog
* Fri Jan 06 chenmaodong <chenmaodong@xfusion.com> v2.2.2-3
- fix: delete k3s cluster with kubenetes version

* Thu Dec 29 2022 chenmaodong <chenmaodong@xfusion.com> v2.2.2-2
- fix invalid syntax of /etc/sysctl.conf after executing init os script

* Mon Aug 8 2022 guofeng <guofeng@yunify.com> v2.2.2-1
- update to latest version v2.2.2

* Tue Sep 14 2021 guofeng <guofeng@yunify.com> v1.2.0-2
- Fixed docker version detection issue
  https://github.com/kubesphere/kubekey/issues/658

* Fri Aug 6 2021 guofeng <guofeng@yunify.com> v1.2.0-1
- Init package
